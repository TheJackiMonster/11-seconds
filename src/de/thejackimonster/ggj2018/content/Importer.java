package de.thejackimonster.ggj2018.content;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import de.thejackimonster.ggj2018.audio.Audio;

public class Importer {
	
	private static final String PATH_ROOT = "/content/";
	
	public static final BufferedImage loadImage(String path, boolean nullable) {
		try {
			final InputStream stream = Importer.class.getResourceAsStream(PATH_ROOT + path + ".png");
			
			if (stream == null) {
				throw new IOException();
			}
			
			return ImageIO.read(stream);
		} catch (IOException e) {
			if (nullable) {
				return null;
			}
		}
		
		return new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
	}
	
	public static final BufferedImage loadImage(String path) {
		return loadImage(path, false);
	}
	
	public static final Audio loadAudio(String path, boolean nullable) {
		AudioFormat format = null;
		byte[] data;
		
		try {
			final URL resource = Importer.class.getResource(PATH_ROOT + path + ".wav");
			
			if (resource == null) {
				throw new IOException();
			}
			
			final AudioInputStream stream = AudioSystem.getAudioInputStream(
				resource
			);
			
			format = stream.getFormat();
			data = new byte[ (int) (stream.getFrameLength() * format.getFrameSize()) ];
			
			stream.read(data, 0, data.length);
			
			stream.close();
		} catch (UnsupportedAudioFileException | IOException e) {
			if (nullable) {
				return null;
			}
			
			data = new byte[0];
		}
		
		return new Audio(format, data);
	}
	
	public static final Audio loadAudio(String path) {
		return loadAudio(path, false);
	}

}

package de.thejackimonster.ggj2018.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import de.thejackimonster.ggj2018.content.Importer;
import de.thejackimonster.ggj2018.game.Game;

public final class Menu {
	
	private static BufferedImage texture = null;
	
	public static final void drawBackground(Graphics gfx, int width, int height) {
		if (texture == null) {
			texture = Importer.loadImage("background");
		}
		
		gfx.drawImage(texture, 0, 0, width, height, null);
	}
	
	public static final void drawTitle(Graphics gfx, int width, int height, int size, Color color, String subtitle) {
		drawTitle(gfx, width, height, size, color);
		
		final int subSize = (size + 1) / 2;
		
		Font.drawString(gfx, subtitle, (width - subtitle.length() * subSize) / 2, (height + size + subSize) / 2, subSize, color);
	}
	
	public static final void drawTitle(Graphics gfx, int width, int height, int size, Color color) {
		Font.drawString(gfx, Game.TITLE, (width - Game.TITLE.length() * size) / 2, (height - size) / 2, size, color);
	}
	
	public static final void drawAuthor(Graphics gfx, int width, int height, int size, Color color) {
		Font.drawString(gfx, "by " + Game.AUTHOR, (width - (Game.AUTHOR.length() + 3) * size) / 2, height - size * 3 / 2, size, color);
	}
	
}

package de.thejackimonster.ggj2018.gui;

import java.awt.Color;

public final class ColorTable {
	
	private static final Color[] COLOR_TABLE = {
		Color.RED,
		Color.ORANGE,
		Color.YELLOW,
		Color.GREEN
	};
	
	public static final Color pickColor(float x, float min, float max) {
		final int value = Math.round((x - min) * COLOR_TABLE.length / (max - min));
		final int index = Math.max(Math.min(value, COLOR_TABLE.length - 1), 0);
		
		return COLOR_TABLE[index];
	}
	
}

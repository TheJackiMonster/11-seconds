package de.thejackimonster.ggj2018.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import de.thejackimonster.ggj2018.content.Importer;

public final class Font {
	
	private static final Map<Color, BufferedImage> textures;
	
	static {
		textures = new HashMap<Color, BufferedImage>();
	}

	private static final String[] FONT_MAP = {
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ",
		"abcdefghijklmnopqrstuvwxyz",
		"0123456789+-*/%&|<>=()[]{}",
		"!?'\"²³\\`´~#_.,:;@€$§µ     "
	};
	
	private static final BufferedImage loadFontTexture(Color color) {
		if (textures.containsKey(color)) {
			return textures.get(color);
		} else {
			final BufferedImage img = Importer.loadImage("font_default");
			
			final int value = color.getRGB();
			final int white = Color.WHITE.getRGB();
			
			final int[] rgb = img.getRGB(
				0, 0, img.getWidth(), img.getHeight(),
				(int[]) null, 0, img.getWidth()
			);
			
			for (int i = 0; i < rgb.length; i++) {
				if (rgb[i] == white) {
					rgb[i] = value;
				}
			}
			
			img.setRGB(
				0, 0, img.getWidth(), img.getHeight(), 
				rgb, 0, img.getWidth()
			);
			
			textures.put(color, img);
			return img;
		}
	}

	public static final void drawChar(Graphics gfx, char ch, int x, int y, int size, Color color) {
		final BufferedImage texture = loadFontTexture(color);
		
		final int clip_y = texture.getHeight() / FONT_MAP.length;
		
		for (int i = 0; i < FONT_MAP.length; i++) {
			final int clip_x = texture.getWidth() / FONT_MAP[i].length();
			
			final int j = FONT_MAP[i].indexOf(ch);
			
			gfx.drawImage(
				texture,
				x, y, x + size, y + size,
				j * clip_x, i * clip_y,
				(j + 1) * clip_x,
				(i + 1) * clip_y,
				null
			);
		}
	}
	
	public static final void drawString(Graphics gfx, String string, int x, int y, int size, Color color) {
		int line_x = 0;
		int line_y = 0;
		
		for (int i = 0; i < string.length(); i++) {
			drawChar(gfx, string.charAt(i), x + line_x * size, y + line_y * size, size, color);
			
			if (string.charAt(i) == '\n') {
				line_x = 0;
				line_y++;
			} else {
				line_x++;
			}
		}
	}

}

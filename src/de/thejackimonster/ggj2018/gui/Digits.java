package de.thejackimonster.ggj2018.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import de.thejackimonster.ggj2018.content.Importer;

public class Digits {
	
	private static final Map<Color, BufferedImage> textures;
	
	static {
		textures = new HashMap<Color, BufferedImage>();
	}
	
	private static final BufferedImage loadDigitsTexture(Color color) {
		if (textures.containsKey(color)) {
			return textures.get(color);
		} else {
			final BufferedImage img = Importer.loadImage("digital_numbers");
			
			final int value = color.getRGB();
			final int white = Color.WHITE.getRGB();
			
			final int[] rgb = img.getRGB(
				0, 0, img.getWidth(), img.getHeight(),
				(int[]) null, 0, img.getWidth()
			);
			
			for (int i = 0; i < rgb.length; i++) {
				if (rgb[i] == white) {
					rgb[i] = value;
				}
			}
			
			img.setRGB(
				0, 0, img.getWidth(), img.getHeight(), 
				rgb, 0, img.getWidth()
			);
			
			textures.put(color, img);
			return img;
		}
	}
	
	public static final void drawDigits(Graphics gfx, int number, int count, int x, int y, int size, Color color) {
		final BufferedImage texture = loadDigitsTexture(color);
		
		final int size_y = size * texture.getHeight() * 10 / texture.getWidth();
		
		int value = number;
		
		for (int i = count - 1; i >= 0; i--) {
			final int index = (value % 10);
			
			value = (value - index) / 10;
			
			gfx.drawImage(
				texture,
				x + i * size,
				y,
				x + (i + 1) * size,
				y + size_y,
				index * texture.getWidth() / 10, 0,
				(index + 1) * texture.getWidth() / 10,
				texture.getHeight(),
				null
			);
		}
	}

}

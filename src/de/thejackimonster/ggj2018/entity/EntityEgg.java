package de.thejackimonster.ggj2018.entity;

import de.thejackimonster.ggj2018.item.Item;
import de.thejackimonster.ggj2018.world.Level;

public final class EntityEgg extends EntityItem {

	public final int eggID;

	public EntityEgg(int id, Level lvl, int ex, int ey) {
		super("egg", Item.EGG_ID, lvl, ex, ey);
		eggID = id;
	}
	
	@Override
	public void trigger(float[] itemStates) {
		itemStates[ID]++;
	}
	
}

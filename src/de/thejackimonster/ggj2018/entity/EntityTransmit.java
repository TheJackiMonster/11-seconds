package de.thejackimonster.ggj2018.entity;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import de.thejackimonster.ggj2018.data.Data;
import de.thejackimonster.ggj2018.world.Level;

public class EntityTransmit extends EntityBox {
	
	private float signal;
	
	public EntityTransmit(Level lvl, int ex, int ey) {
		super("transmit", lvl, ex, ey);
		signal = (float) Math.random();
	}
	
	@Override
	public void update(float deltaTime) {
		signal += deltaTime;
	}

	private boolean test(int dx, int dy) {
		final Data data = level.getData(x + dx, y + dy);
		
		if (!level.getTile(x + dx, y + dy).isSolid(data)) {
			return (level.getEntity(x + dx, y + dy) == null);
		} else {
			return false;
		}
	}
	
	@Override
	public boolean move(int dx, int dy) {
		final List<Entity> linked = new ArrayList<Entity>();
		
		for (final Entity entity : level.allEntities()) {
			if (entity instanceof EntityTransmit) {
				if (!((EntityTransmit) entity).test(dx, dy)) {
					return false;
				} else {
					linked.add((EntityTransmit) entity);
				}
			}
		}
		
		for (final Entity entity : linked) {
			entity.x += dx;
			entity.y += dy;
		}
		
		return true;
	}
	
	@Override
	public void draw(Graphics gfx, int x, int y, int width, int height) {
		super.draw(gfx, x, y, width, height);
		
		gfx.setColor(Color.BLUE);
		
		for (int i = 0; i < 3; i++) {
			final float f = (signal + i * 0.25f) % 1.0f;
			
			final int size_x = Math.round(width * f);
			final int size_y = Math.round(height * 0.5f * f);
			
			gfx.drawOval(x + width / 2 - size_x / 2, y - size_y / 2, size_x, size_y);
		}
	}
	
}

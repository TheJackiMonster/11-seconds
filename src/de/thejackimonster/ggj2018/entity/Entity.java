package de.thejackimonster.ggj2018.entity;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import de.thejackimonster.ggj2018.bonus.Bonus;
import de.thejackimonster.ggj2018.content.Importer;
import de.thejackimonster.ggj2018.data.Data;
import de.thejackimonster.ggj2018.world.Level;

public abstract class Entity {
	
	private static final Map<String, BufferedImage> textures;
	
	static {
		textures = new HashMap<String, BufferedImage>();
	}
	
	public static final BufferedImage pullTexture(String type) {
		if (textures.containsKey(type)) {
			return textures.get(type);
		} else {
			return Importer.loadImage("entity/" + type);
		}
	}

	public static final Entity createEntity(int id, Level level, int x, int y) {
		switch ((id >> 8) & 0xFF) {
		case 0xFF:
			return new EntityPlayer(level, x, y);
		case 0x7F:
			return new EntityBox(level, x, y);
		case 0x42:
			return new EntityClock(level, x, y);
		case 0x87:
			return new EntityHat(level, x, y);
		case 0x16:
			return new EntityCovering(level, x, y);
		case 0xBB:
			return new EntityMonocle(level, x, y);
		case 0x3C:
			return new EntityTransmit(level, x, y);
		case 0x68:
			return new EntityBird(level, x, y);
		default:
			break;
		}
		
		if (Bonus.EASTER_EGGS) {
			final int eggID = ((id >> 16) & 0xFF);
			
			if (eggID > 0) {
				return new EntityEgg((eggID - 1) % Bonus.EGGS.length, level, x, y);
			}
		}
		
		return null;
	}
	
	protected final String type;
	
	protected final Level level;
	
	protected int x;
	protected int y;
	
	protected final BufferedImage texture;
	
	private boolean dead;
	
	protected Entity(String name, Level lvl, int ex, int ey) {
		type = name;
		
		level = lvl;
		
		x = ex;
		y = ey;
		
		texture = pullTexture(type);
		
		dead = false;
	}
	
	public void update(float deltaTime) {}
	
	public void stateUpdate() {}
	
	public void draw(Graphics gfx, int x, int y, int width, int height) {
		gfx.drawImage(
			texture,
			x, y, x+width, y+height,
			0, 0, texture.getWidth(), texture.getHeight(),
			null
		);
	}
	
	protected boolean interact(Entity entity, int dx, int dy) {
		return false;
	}
	
	public boolean move(int dx, int dy) {
		final Data data = level.getData(x + dx, y + dy);
		
		if (!level.getTile(x + dx, y + dy).isSolid(data)) {
			final Entity entity = level.getEntity(x + dx, y + dy);
			
			if (entity != null) {
				return interact(entity, dx, dy);
			} else {
				x += dx;
				y += dy;
				
				return true;
			}
		} else {
			return false;
		}
	}
	
	protected final void kill() {
		dead = true;
	}
	
	public final int getX() {
		return x;
	}
	
	public final int getY() {
		return y;
	}
	
	public final boolean isDead() {
		return dead;
	}

}

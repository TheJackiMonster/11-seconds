package de.thejackimonster.ggj2018.entity;

import de.thejackimonster.ggj2018.item.Item;
import de.thejackimonster.ggj2018.world.Level;

public final class EntityCovering extends EntityItem {

	public EntityCovering(Level lvl, int ex, int ey) {
		super("covering", Item.COVERING_ID, lvl, ex, ey);
	}

	@Override
	public final void trigger(float[] itemStates) {
		itemStates[ID] += 10.0f;
	}

}

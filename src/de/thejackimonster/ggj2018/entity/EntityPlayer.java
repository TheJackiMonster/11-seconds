package de.thejackimonster.ggj2018.entity;

import java.awt.Graphics;

import de.thejackimonster.ggj2018.audio.Sounds;
import de.thejackimonster.ggj2018.bonus.Bonus;
import de.thejackimonster.ggj2018.data.Data;
import de.thejackimonster.ggj2018.data.DataBreak;
import de.thejackimonster.ggj2018.data.DataFinish;
import de.thejackimonster.ggj2018.item.Item;
import de.thejackimonster.ggj2018.world.Level;

public final class EntityPlayer extends Entity {
	
	private final float[] itemStates;

	public EntityPlayer(Level lvl, int ex, int ey) {
		super("player", lvl, ex, ey);
		
		itemStates = new float[Item.TYPES.length];
	}
	
	@Override
	public final void update(float deltaTime) {
		for (int i = 0; i < itemStates.length; i++) {
			if (itemStates[i] > 0.0f) {
				itemStates[i] = Math.max(itemStates[i] - deltaTime, 0.0f);
			}
			
			if ((Bonus.FULL_POWER) && (i != Item.CLOCK_ID)) {
				itemStates[i] = 0.001f;
			}
		}
		
		if (Bonus.NO_TIMER) {
			itemStates[Item.CLOCK_ID] = 0.001f;
		}
	}
	
	@Override
	public final void draw(Graphics gfx, int x, int y, int width, int height) {
		super.draw(gfx, x, y, width, height);
		
		for (int i = 0; i < itemStates.length; i++) {
			if (itemStates[i] > 0.0f) {
				Item.TYPES[i].draw(gfx, x, y, width, height);
			}
		}
	}
	
	@Override
	protected final boolean interact(Entity entity, int dx, int dy) {
		if (entity instanceof EntityBox) {
			if (entity.move(dx, dy)) {
				Sounds.getSound("movement_box").play();
				return true;
			}
		} else
		if (entity instanceof EntityItem) {
			final int itemID = ((EntityItem) entity).ID;
			
			if (itemID >= 0) {
				((EntityItem) entity).trigger(itemStates);
			}
			
			switch (itemID) {
			case Item.CLOCK_ID:
				Sounds.getSound("time").play();
				break;
			case Item.HAT_ID:
				Sounds.getSound("hat").play();
				break;
			case Item.COVERING_ID:
				Sounds.getSound("covering").play();
				break;
			case Item.MONOCLE_ID:
				Sounds.getSound("monocle").play();
				break;
			case Item.EGG_ID:
				Sounds.getSound("egg").play();
				
				if (entity instanceof EntityEgg) {
					Bonus.EGGS[((EntityEgg) entity).eggID].trigger();
				}
				
				break;
			default:
				break;
			}
			
			entity.kill();
			return true;
		} else
		if (entity instanceof EntityBird) {
			Sounds.getSound("bird").play();
			
			Bonus.EASTER_EGGS = true;
			
			entity.kill();
		}
		
		return false;
	}
	
	@Override
	public final boolean move(int dx, int dy) {
		final Data data = level.getData(x + dx, y + dy);
		
		if (!level.getTile(x + dx, y + dy).isSolid(data)) {
			final Entity entity = level.getEntity(x + dx, y + dy);
			
			if ((entity == null) || (interact(entity, dx, dy))) {
				if (entity == null) {
					if ((dx*dx + dy*dy) == 1) {
						Sounds.getSound("movement2").play();
					} else {
						Sounds.getSound("teleport").play();
					}
				}
				
				x += dx;
				y += dy;
				
				return true;
			}
		} else
		if ((data != null) && (data instanceof DataBreak)) {
			if (itemStates[Item.COVERING_ID] > 0.0f) {
				Sounds.getSound("break").play();
				
				((DataBreak) data).destroyed = true;
				
				x += dx;
				y += dy;
				
				return true;
			}
		}
		
		if (((dx*dx + dy*dy) == 1) && (itemStates[Item.MONOCLE_ID] > 0.0f)) {
			return move(dx * 2, dy * 2);
		}
		
		return false;
	}
	
	public final boolean hasWon() {
		final Data data = level.getData(x, y);
		
		if (data instanceof DataFinish) {
			return ((DataFinish) data).open;
		} else {
			return false;
		}
	}
	
	public final float getTime() {
		return Math.max(
			(itemStates[Item.CLOCK_ID] - itemStates[Item.HAT_ID]),
			(itemStates[Item.CLOCK_ID] > 0.0f? 0.001f : 0.0f)
		);
	}

	public final void setTime(float time) {
		itemStates[Item.CLOCK_ID] = time + itemStates[Item.HAT_ID];
	}

}

package de.thejackimonster.ggj2018.entity;

import de.thejackimonster.ggj2018.world.Level;

public class EntityBird extends Entity {
	
	public EntityBird(Level lvl, int ex, int ey) {
		super("bird", lvl, ex, ey);
	}
	
}

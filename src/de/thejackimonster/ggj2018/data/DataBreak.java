package de.thejackimonster.ggj2018.data;

import de.thejackimonster.ggj2018.world.Level;

public class DataBreak extends Data {

	public boolean destroyed;

	public DataBreak(Level lvl, int dx, int dy) {
		super(lvl, dx, dy);
		destroyed = false;
	}

}

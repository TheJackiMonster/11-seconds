package de.thejackimonster.ggj2018.data;

import de.thejackimonster.ggj2018.audio.Sounds;
import de.thejackimonster.ggj2018.entity.Entity;
import de.thejackimonster.ggj2018.world.Level;

public class DataButton extends Data {

	public boolean pressed;
	
	public DataButton(Level lvl, int dx, int dy) {
		super(lvl, dx, dy);
		pressed = false;
	}
	
	@Override
	public void stateUpdate() {
		boolean weightOnIt = false;
		
		for (final Entity entity : level.allEntities()) {
			if ((entity.getX() == x) && (entity.getY() == y)) {
				weightOnIt = true;
				break;
			}
		}
		
		if (weightOnIt != pressed) {
			Sounds.getSound("button").play();
		}
		
		pressed = weightOnIt;
	}

}

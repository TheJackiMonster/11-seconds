package de.thejackimonster.ggj2018.audio;

public final class Music {

	private static Audio lastPlayed = null;
	
	public static final void playMusic(String name) {
		final Audio audio = Sounds.getSound(name);
		
		audio.setLooping(true);
		
		if (lastPlayed != null) {
			lastPlayed.stop();
		}
		
		audio.play();
	}
	
}

package de.thejackimonster.ggj2018.audio;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public final class Audio implements Runnable {
	
	private final AudioFormat format;
	private final byte[] data;

	private final int bufferSize;
	
	private SourceDataLine line;

	private int position;
	private boolean loop;
	
	public Audio(AudioFormat audioFormat, byte[] audioData) {
		format = audioFormat;
		data = audioData;
		
		if (format != null) {
			bufferSize = Math.round(format.getFrameSize() * format.getFrameRate() / 10);
			
			try {
				line = AudioSystem.getSourceDataLine(format);
			} catch (LineUnavailableException e) {
				line = null;
			}
		} else {
			bufferSize = data.length;
		}
		
		position = -1;
		loop = false;
	}
	
	public final void play() {
		final boolean playing = (position > -1);
		
		position = 0;
		
		if ((!playing) && (line != null)) {
			new Thread(this).start();
		}
	}
	
	public final void stop() {
		setLooping(false);
		
		position = data.length;
	}
	
	public final void setLooping(boolean value) {
		loop = value;
	}
	
	public final boolean isLooping() {
		return loop;
	}

	@Override
	public final void run() {
		try {
			line.open(format, bufferSize);
			
			line.start();
			
			do {
				final int size = Math.min(bufferSize, data.length - position);
				
				line.write(data, position, size);
				
				position += size;
				
				if (loop) {
					position = (position % data.length);
				}
			} while (position < data.length);
			
			line.stop();
			line.close();
		} catch (LineUnavailableException e) {
			return;
		}
		
		position = -1;
	}

}

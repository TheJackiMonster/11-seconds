package de.thejackimonster.ggj2018.item;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import de.thejackimonster.ggj2018.entity.Entity;

public final class Item {
	
	public static final Item[] TYPES = {
		new Item("covering", -1.0f / 32, -1.0f / 32, 1.0f),
		new Item("monocle", -2.0f / 32, -1.0f / 32, 1.0f),
		new Item("hat", 0, -17.0f / 32, 1.0f),
		new Item("clock", 0, 0, 0.0f),
		new Item("egg", 0, 0, 0.0f),
	};
	
	public static final int CLOCK_ID = 3;
	public static final int HAT_ID = 2;
	public static final int COVERING_ID = 0;
	public static final int MONOCLE_ID = 1;
	public static final int EGG_ID = 4;

	private final BufferedImage texture;

	private final float offset_x;
	private final float offset_y;

	private final float scale;
	
	public Item(String name, float x, float y, float size) {
		texture = Entity.pullTexture(name);
		
		offset_x = x;
		offset_y = y;
		
		scale = size;
	}

	public final void draw(Graphics gfx, int x, int y, int width, int height) {
		if (scale > 0.0f) {
			gfx.drawImage(
				texture,
				Math.round(x + offset_x * width),
				Math.round(y + offset_y * height),
				Math.round(x + (offset_x + scale) * width),
				Math.round(y + (offset_y + scale) * height),
				0, 0,
				texture.getWidth(),
				texture.getHeight(),
				null
			);
		}
	}

}

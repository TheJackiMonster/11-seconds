package de.thejackimonster.ggj2018.world;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.thejackimonster.ggj2018.content.Importer;
import de.thejackimonster.ggj2018.data.Data;
import de.thejackimonster.ggj2018.entity.Entity;
import de.thejackimonster.ggj2018.entity.EntityPlayer;
import de.thejackimonster.ggj2018.tile.Tile;

public class Level {
	
	public final int width;
	public final int height;
	
	private final int[] ids;
	private final Data[] data;
	
	private final List<Entity> entities;
	
	public final EntityPlayer[] players;
	
	public Level(int id) throws IOException {
		String code = Integer.toHexString(id & 0xFFF).toUpperCase();
		
		while (code.length() < 3) {
			code = "0" + code;
		}
		
		final BufferedImage img = Importer.loadImage(
			"world/level_" + code, true
		);
		
		if (img == null) {
			throw new IOException("Level missing! ( " + id + " )");
		}
		
		width = img.getWidth();
		height = img.getHeight();
		
		ids = img.getRGB(0, 0, img.getWidth(), img.getHeight(), (int[]) null, 0, img.getWidth());
		data = new Data[ids.length];
		
		entities = new ArrayList<Entity>();
		
		int player_index = 0;
		
		for (int i = 0; i < data.length; i++) {
			final int x = i % width;
			final int y = i / width;
			
			data[i] = Tile.getTile(ids[i]).createData(this, x, y);
			
			final Entity entity = Entity.createEntity(ids[i], this, x, y);
			
			if (entity != null) {
				entities.add(entity);
				
				if (entity instanceof EntityPlayer) {
					player_index++;
				}
			}
		}
		
		players = new EntityPlayer[player_index];
		player_index = 0;
		
		for (final Entity entity : entities) {
			if (entity instanceof EntityPlayer) {
				players[player_index++] = (EntityPlayer) entity;
			}
		}
	}
	
	public final void update(float deltaTime) {
		synchronized (entities) {
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					if (data[x + y * width] != null) {
						data[x + y * width].update(deltaTime);
					}
				}
			}
			
			for (final Entity entity : entities) {
				entity.update(deltaTime);
			}
		}
	}
	
	public final void stateUpdate() {
		synchronized (entities) {
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					if (data[x + y * width] != null) {
						data[x + y * width].stateUpdate();
					}
				}
			}
			
			for (int i = entities.size() - 1; i >= 0; i--) {
				if (entities.get(i).isDead()) {
					entities.remove(i);
				} else {
					entities.get(i).stateUpdate();
				}
			}
		}
	}
	
	private final int getTileId(int x, int y) {
		if ((x >= 0) && (x < width) && (y >= 0) && (y < height)) {
			return ids[x + y * width];
		} else {
			return 0x00000000;
		}
	}
	
	public final Tile getTile(int x, int y) {
		return Tile.getTile(getTileId(x, y));
	}
	
	public final Data getData(int x, int y) {
		if ((x >= 0) && (x < width) && (y >= 0) && (y < height)) {
			return data[x + y * width];
		} else {
			return null;
		}
	}
	
	public final Entity getEntity(int x, int y) {
		for (final Entity entity : entities) {
			if ((entity.getX() == x) && (entity.getY() == y)) {
				return entity;
			}
		}
		
		return null;
	}
	
	public final Iterable<Data> allData() {
		return () -> {
			return new Iterator<Data>() {
				
				private int x = 0;
				private int y = 0;

				private final void gotoNext() {
					x++;
					
					if (x >= width) {
						x = 0;
						y++;
					}
				}
				
				@Override
				public final boolean hasNext() {
					if ((x >= width) || (y >= height)) {
						return false;
					}
					
					if (data[x + y * width] == null) {
						gotoNext();
						
						return hasNext();
					} else {
						return true;
					}
				}

				@Override
				public final Data next() {
					final int index = x + y * width;
					
					gotoNext();
					
					return data[index];
				}
				
			};
		};
	}

	public final Iterable<Entity> allEntities() {
		return entities;
	}

}

package de.thejackimonster.ggj2018.tile;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import de.thejackimonster.ggj2018.content.Importer;
import de.thejackimonster.ggj2018.data.Data;
import de.thejackimonster.ggj2018.world.Level;

public class Tile {
	
	private static final Map<Integer, Tile> TILES = new HashMap<Integer, Tile>();
	
	public static final Tile WALL;
	public static final Tile FLOOR;
	
	static {
		TILES.put(0x00, WALL = new Tile("wall2", true));
		TILES.put(0xFF, FLOOR = new Tile("floor2", false));
		TILES.put(0x53, new TileFinish());
		TILES.put(0x9D, new TileButton());
		TILES.put(0x16, new TileBreak("broken_wall2"));
		TILES.put(0xA2, new TileBreak("broken_body"));
	}
	
	public static final Tile getTile(int id) {
		if (TILES.containsKey(id & 0xFF)) {
			return TILES.get(id & 0xFF);
		} else {
			return WALL;
		}
	}
	
	protected final boolean solid;
	
	protected final BufferedImage texture;
	
	protected Tile(String name, boolean solidState) {
		solid = solidState;
		
		texture = Importer.loadImage("tile/" + name);
	}
	
	public Data createData(Level level, int x, int y) {
		return new Data(level, x, y);
	}
	
	public void draw(Graphics gfx, int x, int y, int width, int height, Data data) {
		final int detail = (Data.getDetail(data) & 3);
		
		gfx.drawImage(
			texture,
			x, y, x+width, y+height,
			(detail % 2) * texture.getWidth() / 2,
			(detail / 2) * texture.getHeight() / 2,
			((detail % 2) + 1) * texture.getWidth() / 2,
			((detail / 2) + 1) * texture.getHeight() / 2,
			null
		);
	}
	
	public boolean isSolid(Data data) {
		return solid;
	}

}

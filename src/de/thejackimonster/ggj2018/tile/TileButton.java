package de.thejackimonster.ggj2018.tile;

import java.awt.Graphics;

import de.thejackimonster.ggj2018.data.Data;
import de.thejackimonster.ggj2018.data.DataButton;
import de.thejackimonster.ggj2018.world.Level;

public class TileButton extends Tile {

	public TileButton() {
		super("button2", false);
	}
	
	@Override
	public final Data createData(Level level, int x, int y) {
		return new DataButton(level, x, y);
	}
	
	@Override
	public final void draw(Graphics gfx, int x, int y, int width, int height, Data data) {
		Tile.FLOOR.draw(gfx, x, y, width, height, data);
		super.draw(gfx, x, y, width, height, data);
	}

}

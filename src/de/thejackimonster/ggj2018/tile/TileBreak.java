package de.thejackimonster.ggj2018.tile;

import java.awt.Graphics;

import de.thejackimonster.ggj2018.data.Data;
import de.thejackimonster.ggj2018.data.DataBreak;
import de.thejackimonster.ggj2018.world.Level;

public class TileBreak extends Tile {

	public TileBreak(String name) {
		super(name, true);
	}
	
	public Data createData(Level level, int x, int y) {
		return new DataBreak(level, x, y);
	}
	
	public void draw(Graphics gfx, int x, int y, int width, int height, Data data) {
		Tile.FLOOR.draw(gfx, x, y, width, height, data);
		
		if (!((DataBreak) data).destroyed) {
			super.draw(gfx, x, y, width, height, data);
		}
	}
	
	public boolean isSolid(Data data) {
		return !((DataBreak) data).destroyed;
	}

}

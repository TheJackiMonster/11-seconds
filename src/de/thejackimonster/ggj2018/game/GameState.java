package de.thejackimonster.ggj2018.game;

public enum GameState {
	
	INIT_MENU(false, false, false),
	START_MENU(false, false, false),
	HELP_MENU(false, false, false),
	WAIT_MENU(false, true, true),
	
	INGAME(true, true, true),
	
	BONUS_MENU(false, false, false);
	
	public final boolean doUpdates;
	public final boolean doStateUpdates;
	public final boolean doRendering;
	
	private GameState(boolean updates, boolean stateUpdates, boolean rendering) {
		doUpdates = updates;
		doStateUpdates = stateUpdates;
		doRendering = rendering;
	}
	
}
